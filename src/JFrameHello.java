
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class JFrameHello extends JFrame{

    public static void main(String[] args) {
        JFrame tela = new JFrame("Hello Swing");
        tela.setBounds(0, 0, 400, 200);
        tela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        tela.setLayout(new FlowLayout());
        
        JLabel jlabel = new JLabel("Hello Swing");
        
        JTextField field = new JTextField();
        field.setColumns(10);
        
        CliqueBtao click = new CliqueBtao();
        
        JButton button = new JButton("Há");
        button.addMouseListener(click);
        
        tela.add(jlabel);
        tela.add(field);
        tela.add(button);
        
        tela.pack();
        tela.setVisible(true);
    }   
}

